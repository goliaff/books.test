-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 22, 2015 at 11:13 AM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `books`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE IF NOT EXISTS `authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`id`, `firstname`, `lastname`) VALUES
(1, 'Дональд', 'Дак'),
(2, 'Николай', 'Гоголь'),
(3, 'John', 'Doe');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `date` date NOT NULL,
  `author_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`author_id`),
  KEY `fk_books_authors_idx` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `name`, `date_create`, `date_update`, `preview`, `date`, `author_id`) VALUES
(1, 'Мёртвые души', '2015-08-20 17:07:33', '2015-08-22 08:11:28', 'images/dead_souls.jpg', '2013-09-13', 2),
(2, 'Book1', '2015-08-20 21:00:00', NULL, 'images/dead_souls.jpg', '2013-09-09', 1),
(3, 'Book2', '2015-08-20 21:00:00', NULL, 'images/dead_souls.jpg', '2013-09-03', 3),
(4, 'Best Book247', '2015-08-20 21:00:00', '2015-08-22 08:10:22', 'images/dead_souls.jpg', '2013-05-14', 2),
(5, 'End Book2', '2015-08-20 21:00:00', NULL, 'images/dead_souls.jpg', '2013-01-07', 3),
(6, 'Главная Book1', '2015-08-20 21:00:00', NULL, 'images/dead_souls.jpg', '2014-02-28', 1),
(7, 'Главная Book2', '2015-08-20 21:00:00', NULL, 'images/dead_souls.jpg', '2014-04-23', 1),
(8, 'The Best Book2', '2015-08-20 21:00:00', NULL, 'images/dead_souls.jpg', '2012-09-03', 2),
(9, 'Zend End Book2', '2015-08-20 21:00:00', NULL, 'images/dead_souls.jpg', '2012-07-23', 3);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `fk_books_authors` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
