<?php

namespace app\controllers;

use app\models\Authors;
use app\models\BooksSearch;
use Yii;
use app\models\Books;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BooksController implements the CRUD actions for Books model.
 */
class BooksController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'update', 'delete', 'view',],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Books models.
     * @return mixed
     */
    public function actionIndex()
    {
        Authors::getAllAuthors();
        $searchModel = new BooksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $searchModel,
        ]);
    }

    /**
     * Displays a single Books model.
     * @param integer $id
     * @param integer $author_id
     * @return mixed
     */
    public function actionView($id, $author_id)
    {
        if (!Yii::$app->request->isAjax) {
            return $this->render('view', [
                'model' => $this->findModel($id, $author_id),
            ]);
        } else {
            return $this->renderAjax('_view', [
                'model' => $this->findModel($id, $author_id),
            ]);
        }
    }


    /**
     * Updates an existing Books model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $author_id
     * @return mixed
     */
    public function actionUpdate($id, $author_id)
    {
        $model = $this->findModel($id, $author_id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Url::previous('index'));
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Books model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $author_id
     * @return mixed
     */
    public function actionDelete($id, $author_id)
    {
        $this->findModel($id, $author_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Books model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $author_id
     * @return Books the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $author_id)
    {
        if (($model = Books::findOne(['id' => $id, 'author_id' => $author_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        if ($action->id === 'update' && !Yii::$app->request->post()) {
            Url::remember(Yii::$app->request->referrer, 'index');
        }

        return true;
    }
}
