<?php

use kartik\date\DatePicker;
use newerton\fancybox\FancyBox;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider
/* @var $searchModel yii\data\ActiveDataProvider */

$this->title = 'Книги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(['action' => '', 'method' => 'GET']); ?>


    <div class="row">
        <div class="col-md-8">
            <div class="col-xs-4">
                <?= $form->field($model, 'author_id')->dropDownList(\app\models\Authors::getAllAuthors(),
                    ['prompt' => 'Автор'])->label(''); ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($model,
                    'name')->textInput([
                    'placeholder' => $model->getAttributeLabel('name'),
                    'maxlength' => 255
                ])->label(''); ?>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div>
            <div class="col-xs-3">
                <?= $form->field($model, 'publishedFrom')->widget(DatePicker::className(),
                    [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd/mm/yyyy'
                        ]
                    ])->label('Дата выхода книги от:'); ?>
            </div>
            <div class="col-xs-3">
                <?= $form->field($model, 'publishedTo')->widget(DatePicker::className(),
                    [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd/mm/yyyy'
                        ]
                    ])->label('Дата выхода книги до:'); ?>
            </div>
            <?= Html::submitButton('Искать',
                ['class' => 'btn btn-success', 'style' => 'margin-top:25px;float:right;']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
            [
                'attribute' => 'preview',
                'enableSorting' => false,
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(Html::img(Yii::getAlias('@web') . '/' . $data['preview'], ['height' => '50px']),
                        Yii::getAlias('@web') . '/' . $data['preview'], ['rel' => 'fancybox']);
                }
            ],
            'date:date',
            'authorFullName',
            'date_create:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Кнопки действий',
                'buttons' => [
                    'view' => function ($url, $data) {
                        $link = Html::a('<span class="glyphicon glyphicon-eye-open"></span>', [$url], [
                            'title' => Yii::t('yii', 'View'),
                            'class' => 'modalView',
                            'data' => [
                                'target' => '#myModal',
                                'toggle' => 'modal',
                                'backdrop' => 'static',
                            ]
                        ]);

                        return $link;
                    },
                ],
                'template' => '{update} {view} {delete}'
            ],
        ],
    ]); ?>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true"></div>

    <?= FancyBox::widget([
        'target' => 'a[rel=fancybox]',
        'helpers' => true,
        'mouse' => true,
        'config' => [
            'maxWidth' => '90%',
            'maxHeight' => '90%',
            'playSpeed' => 7000,
            'padding' => 0,
            'fitToView' => false,
            'width' => '70%',
            'height' => '70%',
            'autoSize' => false,
            'closeClick' => false,
            'openEffect' => 'elastic',
            'closeEffect' => 'elastic',
            'prevEffect' => 'elastic',
            'nextEffect' => 'elastic',
            'closeBtn' => false,
            'openOpacity' => true,
            'helpers' => [
                'title' => ['type' => 'float'],
                'overlay' => [
                    'css' => [
                        'background' => 'rgba(0, 0, 0, 0.8)'
                    ]
                ]
            ],
        ]
    ]);
    ?>
</div>
