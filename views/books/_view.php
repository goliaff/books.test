<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Books */

$this->title = $model->name;
?>
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><?= Html::encode($this->title) ?></h4>
        </div>
        <div class="modal-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'name',
                    'authorFullName',
                    'date:date',
                    'date_create:datetime',
                    'date_update:datetime',
                    'preview',
                ],
            ]) ?>
        </div>
        <div class="modal-footer">
            <?= Html::a('Редактировать', ['update', 'id' => $model->id, 'author_id' => $model->author_id],
                ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id, 'author_id' => $model->author_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы уверены что хотите удалить эту книгу?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
    </div>
</div>
