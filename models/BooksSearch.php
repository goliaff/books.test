<?php

namespace app\models;


use yii\data\ActiveDataProvider;

class BooksSearch extends Books
{
    public $author_id;
    public $name;
    public $publishedFrom;
    public $publishedTo;

    public function rules()
    {
        return [
            [['author_id', 'name', 'publishedFrom', 'publishedTo'], 'safe'],
            [['author_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['publishedFrom', 'publishedTo'], 'date', 'format' => 'd/M/yyyy'],
        ];
    }

    public function search($params)
    {
        $query = Books::find();
        $query->joinWith(['author']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'name',
                'date',
                'date_create',
                'authorFullName' => [
                    'asc' => ['authors.firstname' => SORT_ASC, 'authors.lastname' => SORT_ASC],
                    'desc' => ['authors.firstname' => SORT_DESC, 'authors.lastname' => SORT_DESC],
                ]
            ]
        ]);

        if (!$this->load($params)) {
            return $dataProvider;
        }

        $query->andFilterWhere(['author_id' => $this->author_id]);
        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['>', 'date', $this->convertDate($this->publishedFrom)]);
        $query->andFilterWhere(['<', 'date', $this->convertDate($this->publishedTo)]);

        return $dataProvider;
    }

    private function convertDate($date)
    {
        $converted = $date;
        if($date){
            $date = explode('/', $date);
            $converted = $date[2] . '-' . $date[1] . '-' . $date[0];
        }

        return $converted;
    }
}