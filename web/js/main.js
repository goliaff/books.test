$(document).ready(function () {
    var body = $('body');
    var modal = $('#myModal');
    body.on('hidden.bs.modal', function () {
        modal.html('');
    });

    body.on('click', '.modalView', function () {
        var url = $(this).attr('href');
        modal.html('<div class="loading"></div>');
        $.ajax({
            url: url,
            method: 'GET',
            success: function (data) {
                modal.html(data);
            }
        });
    });
});
